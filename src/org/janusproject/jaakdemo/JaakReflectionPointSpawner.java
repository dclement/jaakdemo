
/*
 * $Id$
 * 
 * Janus platform is an open-source multiagent platform.
 * More details on <http://www.janus-project.org>
 * Copyright (C) 2010-2011 Janus Core Developers
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.janusproject.jaakdemo;

import org.arakhne.afc.math.MathUtil;
import org.janusproject.jaakdemo.organization.Ant;
import org.janusproject.jaakdemo.organization.Patroller;
import org.janusproject.jaak.envinterface.body.TurtleBody;
import org.janusproject.jaak.spawner.JaakPointSpawner;
import org.janusproject.jaak.turtle.Turtle;
import org.janusproject.kernel.address.AgentAddress;
import org.janusproject.kernel.time.KernelTimeManager;
import org.janusproject.kernel.util.random.RandomNumber;

/** Spawner for the Ant Colony Demo.
 * 
 * @author $Author: cdalle$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
public class JaakReflectionPointSpawner extends JaakPointSpawner {

	private int number;
	/**
	 * 
	 * @param turtleType class of the turtle to spawn
	 * @param budget is the maximal count of ants to spawn.
	 * @param x is the spawning position.
	 * @param y is the spawning position.
	 */
	public JaakReflectionPointSpawner(Class<? extends Turtle> turtleType, int budget, int x, int y) {
		super(x,y);
		this.number = budget;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean isSpawnable(KernelTimeManager timeManager) {
		return (this.number>0);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Turtle createTurtle(KernelTimeManager timeManager) {
		assert(this.number>0);
		return new Ant();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void turtleSpawned(Turtle turtle, TurtleBody body, KernelTimeManager timeManager) {
		if (this.number>0) {
			--this.number;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void turtleSpawned(AgentAddress turtle, TurtleBody body, KernelTimeManager timeManager) {
		//
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Object[] getTurtleActivationParameters(Turtle turtle, KernelTimeManager timeManager) {
		assert(this.number>0);
		if (this.number>0) {
			return new Object[] { Patroller.class };
		}
		return new Object[0];
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected float computeSpawnedTurtleOrientation(KernelTimeManager timeManager) {
		return RandomNumber.nextFloat() * MathUtil.TWO_PI;
	}

}
