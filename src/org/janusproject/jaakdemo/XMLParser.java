package org.janusproject.jaakdemo;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.arakhne.afc.math.discrete.object2d.Point2i;
import org.arakhne.afc.vmutil.locale.Locale;
import org.janusproject.jaak.envinterface.channel.GridStateChannel;
import org.janusproject.jaak.envinterface.perception.EnvironmentalObject;
import org.janusproject.jaak.environment.model.JaakEnvironment;
import org.janusproject.jaak.environment.solver.ActionApplier;
import org.janusproject.jaak.spawner.JaakSpawner;
import org.janusproject.jaak.turtle.Turtle;
import org.janusproject.jaakdemo.environment.AntColonySpawner;
import org.janusproject.kernel.address.AgentAddress;
import org.janusproject.kernel.agent.Kernels;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.content.Context;
import android.graphics.Point;
import android.util.Log;



/* 
 * $Id$
 * 
 * Janus platform is an open-source multiagent platform.
 * More details on <http://www.janus-project.org>
 * Copyright (C) 2010-2012 Janus Core Developers
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/** ReadXml : read information on the xml to launch application
 * 			Create environment and spawners
 * 
 * @author $Author: cdalle$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */



public class XMLParser{

	public int CELL_SIZE = 4;
	public static int ANT_COLONY_COUNT =0;
	public int ANT_COLONY_PATROLLER_POPULATION = 10;	
	public int ANT_COLONY_FORAGER_POPULATION = 50;	
	public int FOOD_SOURCE = 30;
	public static int MAX_FOOD_PER_SOURCE = 1000;
	private boolean isWrappedEnvironment = true;
	public JaakEnvironment environment = null;
	public List<JaakSpawner> createdSpawners = new ArrayList<JaakSpawner>();
	public JaakSpawner[] spawner = null;
	public ArrayList<Element> listBurrow = new ArrayList<Element>();
	public ArrayList<Element> listSpawner= new ArrayList<Element>();
	//Dimension of world simulation
	public int WIDTH = 300;
	public int HEIGHT = 200;
	//Device dimension
	public Point deviceSize = null;
	private boolean isRead = false;
	private Context myContext =null;
	/** Max pheromone amount.
	 */
	public static final float MAX_PHEROMONE_AMOUNT = 10f;
	
	/**
	 * Class constructor
	 * @param Context is the context of the application
	 */
	public XMLParser(Context context) 
	{
		myContext = context;
	}

	/**
	 * @param is is the document to parse
	 * @param size is the dimension of the device
	 */
	public void readXML(InputStream is, Point size ) throws IOException, ParserConfigurationException, SAXException {
		this.isRead = true;
		deviceSize = size;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(is);
		doc.getDocumentElement ().normalize ();
		positions.clear();
		xmlConfig(doc);
	}

	/**
	 * Start parsing the document given in parameter
	 * Begin to parse after the root node
	 * Create Ant population
	 * @param doc is the XML File
	 */
	private void xmlConfig(Document doc) throws IOException 
	{
		xmlEnvironment(doc.getElementsByTagName("environment"));
		xmlSpawner(doc.getElementsByTagName("spawner"));
		
		//Create Ant population
		createBurrow();
		
		xmlTime(doc.getElementsByTagName("time"));
		spawner = new JaakSpawner[createdSpawners.size()];
		//Burrow are in the beginning of the tab other spawner follow
		for(int i=0;i<createdSpawners.size();i++)
		{
			spawner[i]=createdSpawners.get(i);
		}
	}


	/**
	 * Create the environment with the information given in the node environment
	 * It's call methods for each subnode from environment'snode
	 */
	private void xmlEnvironment(NodeList nodeList) throws IOException 
	{
		for(int s=0;s<nodeList.getLength();s++)
		{
			Node element = nodeList.item(s);
			Element elementEnv = (Element)element;
			try
			{
				WIDTH = Integer.valueOf(elementEnv.getAttribute("width"));
				HEIGHT = Integer.valueOf(elementEnv.getAttribute("height"));
			}
			catch (NumberFormatException e)
			{
				Log.i("xmlEnvironment","Environment dimension not correct --> set to device");
				WIDTH = deviceSize.x/CELL_SIZE;
				HEIGHT = deviceSize.y/CELL_SIZE;
			}
			//World should take a least the resolution of the device
			if(WIDTH<deviceSize.x/CELL_SIZE)
			{
				WIDTH=deviceSize.x/CELL_SIZE;
			}
			if(HEIGHT<deviceSize.y/CELL_SIZE)
			{
				HEIGHT=deviceSize.y/CELL_SIZE;
			}
			isWrappedEnvironment = Boolean.valueOf(elementEnv.getAttribute("wrapped"));
			environment = createEnvironment(WIDTH, HEIGHT);
			if (element.hasChildNodes())
			{
				NodeList listChild = element.getChildNodes();
				for (int i=0; i < listChild.getLength(); i++) {
					Node subnode = listChild.item(i);
					if (subnode.getNodeType() == Node.ELEMENT_NODE) {
						if (subnode.getNodeName().equals("gui")) 
						{
							if(subnode.hasChildNodes())
							{
								xmlGui(subnode.getChildNodes());
							}
						}
						if (subnode.getNodeName().equals("substance")) 
						{
							if(subnode.hasChildNodes())
							{
								xmlSubstance(subnode);
							}
						}
						if (subnode.getNodeName().equals("burrow")) 
						{
							if(subnode.hasChildNodes())
							{
								xmlBurrow(subnode);
							}
						}
						if (subnode.getNodeName().equals("object")) 
						{
							if(subnode.hasChildNodes())
							{
								xmlObject(subnode);
							}
						}
					}
				}
			}
		}		
	}

	private void xmlSpawner(NodeList nodeList) throws IOException 
	{
		for(int s=0;s<nodeList.getLength();s++)
		{
			Node element = nodeList.item(s);
			if (element.hasChildNodes())
			{
				NodeList listChild = element.getChildNodes();
				for (int i=0; i < listChild.getLength(); i++) {
					Node subnode = listChild.item(i);
					if (subnode.getNodeType() == Node.ELEMENT_NODE) {
						if (subnode.getNodeName().equals("spawn")) 
						{
							if(subnode.hasChildNodes())
							{
								xmlSpawn(subnode);
							}
						}
					}
				}
			}
		}
	}

	private void xmlTime(NodeList nodeList) throws IOException 
	{
		for(int s=0;s<nodeList.getLength();s++)
		{
			Node element = nodeList.item(s);
			Element elementEnv = (Element)element;
			String val = elementEnv.getAttribute("deltaTime");
			String val1 = elementEnv.getAttribute("startAt");
			Log.i("deltaTime",val);
			Log.i("startAt",val1);
		}
	}

	private void xmlGui(NodeList childNodes) 
	{	
		for (int j=0; j < childNodes.getLength(); j++) {
			Node subsubnode = childNodes.item(j);
			if (subsubnode.getNodeType() == Node.ELEMENT_NODE) {
				if (subsubnode.getNodeName().equals("background")) 
				{
					Element elementsubsubnode = (Element)subsubnode;
					String el1 = elementsubsubnode.getAttribute("color");
					String el2 = elementsubsubnode.getAttribute("image");
					Log.i("el1",el1);
					Log.i("el2",el2);
				}
			}
		}

	}

	/**
	 * Create food on the environment with the information given in the XML
	 * Nodes is the node with the name substance
	 */
	private void xmlSubstance(Node Nodes)
	{
		Node element = Nodes;
		Element elementEnv = (Element)element;
		String val = elementEnv.getAttribute("type");
		String val1 = elementEnv.getAttribute("quantity");
		String val2 = elementEnv.getAttribute("position");
		String val3 = elementEnv.getAttribute("maxSubstance");
		String val4 = elementEnv.getAttribute("substance");
		String val5 = elementEnv.getAttribute("id");
		Log.i("type",val);
		Log.i("quantity",val1);
		Log.i("position",val2);
		Log.i("maxSubstance",val3);
		Log.i("substance",val4);
		Log.i("id",val5);

		//Control the value of a integer for quantity and maxSubstance
		try
		{
			foodEnvironment(getJaakEnvironment(), Integer.valueOf(elementEnv.getAttribute("quantity")), Integer.valueOf(elementEnv.getAttribute("maxSubstance")),elementEnv.getAttribute("type"),elementEnv.getAttribute("position"));
		}
		catch(NumberFormatException e)
		{
			Log.i("XmlSubstance", "Quantity or maxSubstance isn't integer --> set 1 and 1000");
			foodEnvironment(getJaakEnvironment(), 1, 1000,elementEnv.getAttribute("type"),elementEnv.getAttribute("position"));
		}
	}

	private void xmlBurrow(Node Nodes)
	{
		Node element = Nodes;
		Element elementEnv = (Element)element;
		String val = elementEnv.getAttribute("type");
		String val1 = elementEnv.getAttribute("quantity");
		String val2 = elementEnv.getAttribute("position");
		String val3 = elementEnv.getAttribute("id");
		Log.i("type",val);
		Log.i("quantity",val1);
		Log.i("position",val2);
		Log.i("id",val3);
		listBurrow.add(elementEnv);
	}

	private void xmlObject(Node Nodes)
	{
		Node element = Nodes;
		Element elementEnv = (Element)element;
		String val = elementEnv.getAttribute("type");
		String val1 = elementEnv.getAttribute("quantity");
		String val2 = elementEnv.getAttribute("position");
		String val3 = elementEnv.getAttribute("id");
		Log.i("type",val);
		Log.i("quantity",val1);
		Log.i("position",val2);
		Log.i("id",val3);
	}	

	private void xmlSpawn(Node Nodes)
	{
		Node element = Nodes;
		Element elementEnv = (Element)element;
		String val1 = elementEnv.getAttribute("quantity");
		String val2 = elementEnv.getAttribute("type");
		Log.i("quantity",val1);
		Log.i("type",val2);
		listSpawner.add(elementEnv);
	}

	/**Function call to get attribute of spawn to create burrow
	 * or create spawn at location given etc.
	 *  @param nodelist is the node to parse
	 *  @param type is the class name of the spawner
	 *  @param SpawnerNotInBurrow : false if spawner is in the burrow, true in other case
	 */
	
	private String[] xmlLocation(NodeList Nodeslist, String type, boolean SpawnerNotInBurrow)
	{
		String[] out = new String[3];
		for (int j=0; j < Nodeslist.getLength(); j++) {
			Node subsubnode = Nodeslist.item(j);
			if (subsubnode.getNodeType() == Node.ELEMENT_NODE) {
				if (subsubnode.getNodeName().equals("location")) 
				{
					Element elementsubsubnode = (Element)subsubnode;
					NodeList subNodesList = elementsubsubnode.getChildNodes();
					for (int k=0; k < subNodesList.getLength(); k++) {
						Node subsubsubnode = subNodesList.item(j);
						if (subsubsubnode.getNodeType() == Node.ELEMENT_NODE) {
							if(!SpawnerNotInBurrow)
							{
								if (subsubsubnode.getNodeName().equals("entity")) 
								{
									Element El = (Element)subsubsubnode;
									out[0]=El.getAttribute("id");
									out[1]=El.getAttribute("job");
									out[2]=El.getAttribute("entity");
									return out;
								}
							}
							else
							{
								if (subsubsubnode.getNodeName().equals("oncell")) 
								{
									Element El = (Element)subsubsubnode;
									out[0]=El.getAttribute("row");
									out[1]=El.getAttribute("column");
									out[2]="oncell";
									return out;
								}
							}
						} 

					}
				}
			}
		}
		return null;
	}

	/**Get the number ant patroller on a burrow
	 * 
	 * @param nameBurrow : idvalue of the burrow
	 */
	private int numberPatroller(String nameBurrow)
	{
		int number =0;
		for(int i = 0;i<listSpawner.size();i++)
		{
			Element element = listSpawner.get(i);
			String[] profil = xmlLocation(element.getChildNodes(),element.getAttribute("type"),false);
			if(profil!=null && profil[0].equalsIgnoreCase(nameBurrow))
			{
				if(profil[1].equalsIgnoreCase("patroller"))
				{
					try
					{
						number = number + Integer.valueOf(element.getAttribute("quantity"));
					}
					catch(NumberFormatException e)
					{
						Log.i("numberPatroller","set --> 0");
						number =0;
					}
				}
			}
		}
		return number;
	}

	/**Get the number ant forager on a burrow
	 * 
	 * @param nameBurrow : idvalue of the burrow
	 */
	private int numberForager(String nameBurrow)
	{
		int number =0;
		for(int i = 0;i<listSpawner.size();i++)
		{
			Element element = listSpawner.get(i);
			String[] profil = xmlLocation(element.getChildNodes(),element.getAttribute("type"),false);
			if(profil!=null && profil[0].equalsIgnoreCase(nameBurrow))
			{
				if(profil[1].equalsIgnoreCase("forager"))
				{
					try
					{
						number = number + Integer.valueOf(element.getAttribute("quantity"));
					}
					catch(NumberFormatException e)
					{
						Log.i("numberForager","set --> 0");
						number =0;
					}
				}
			}
		}
		return number;
	}

	/**Function to create ant population
	 * 
	 * @param env : environment where ant are create
	 * @param type : class name of the ant
	 * @param row : x-position in the env 
	 * @param column : y-position in the env
	 * @param quantity : number of ant to create
	 */
	private void createAnt(JaakEnvironment env, String type, String row, String column, String quantity)
	{
		ActionApplier actionApplier = env.getActionApplier();
		Class<? extends Turtle> turtleType;
		Constructor<? extends Turtle> tmp = null;
		try {
			Class<?> cls = Class.forName(type);
			if (!Turtle.class.isAssignableFrom(cls)) {
				System.err.println("Not a turtle: "+cls);
				return;
			}
			turtleType = (Class<? extends Turtle>)cls;
			try {
				tmp = turtleType.getDeclaredConstructor();
				tmp.setAccessible(true);
			} catch (NoSuchMethodException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				return;
			}
		}
		catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			Log.i("FoodEnvironment","Class Not found");
			return;
		}

		Point2i p =null;
		try
		{
			p = new Point2i(Integer.valueOf(row),Integer.valueOf(column));	

			if(Integer.valueOf(row)<0 || Integer.valueOf(row)>environment.getWidth())
			{
				p = new Point2i(0,Integer.valueOf(column));
				if(Integer.valueOf(column)<0 || Integer.valueOf(column)>environment.getHeight())
				{
					p = new Point2i(0,0);
				}
			}
			if(Integer.valueOf(column)<0 || Integer.valueOf(column)>environment.getHeight())
			{
				p = new Point2i(Integer.valueOf(row),0);
				if(Integer.valueOf(row)<0 || Integer.valueOf(row)>environment.getWidth())
				{
					p = new Point2i(0,0);
				}
			}

		}
		catch(NumberFormatException e)
		{
			Log.i("createAnt","number not correct -->set to 0");
			return;
		}
		
		int Count = 0;
		try
		{
			Count = Integer.valueOf(quantity);
		}
		catch (NumberFormatException e)
		{
			Log.i("CreateAnt","Quantity is not a number : --> set to 0");
		}
		
		JaakReflectionPointSpawner newSpawner = new JaakReflectionPointSpawner(turtleType, Count, p.x(), p.y());
		createdSpawners.add(newSpawner);
	}
	
	public List<JaakSpawner> getSpawners() {
		return this.createdSpawners;
	}
	
	public JaakEnvironment getJaakEnvironment() {
		return environment;
	}

	public JaakSpawner[] getJaakSpawners() {
		return this.spawner;
	}

	
	private static final Set<Point2i> positions = new TreeSet<Point2i>(new Comparator<Point2i>(){
		@Override
		public int compare(Point2i o1, Point2i o2) {
			if (o1==o2) return 0;
			if (o1==null) return Integer.MIN_VALUE;
			if (o2==null) return Integer.MAX_VALUE;
			int cmp = o1.x() - o2.x();
			if (cmp!=0) return cmp;
			return o1.y() - o2.y();
		}
	});


	/**
	 * Create an instance of the environment.
	 * @param width : width dimension of the env
	 * @param height : height dimension of the env
	 * @return an instance of the environment.
	 */
	public JaakEnvironment createEnvironment(int width,int height) {
		JaakEnvironment environment = new JaakEnvironment(width, height);
		environment.setWrapped(isWrappedEnvironment);
		return environment;
	}

	/**
	 * Create the substance from type given (food)
	 * @param env : environment of the application
	 * @param FOOD_SOURCE : number of food
	 * @param MAX_FOOD_PER_SOURCES : number max of one food
	 * @param type : class name of the substance
	 * @param position : position of the substance random|x;y
	 */
	public static void foodEnvironment(JaakEnvironment env, int FOOD_SOURCE, int MAX_FOOD_PER_SOURCES, String type, String position)
	{
		Random rnd = new Random();
		ActionApplier actionApplier = env.getActionApplier();

		Class<?> cls = null;
		try {
			cls = Class.forName(type);

			Class[] paramPerso = new Class[1];	
			paramPerso[0] = Float.TYPE;
			Constructor<?> tmp = null;
			try {
				tmp = cls.getDeclaredConstructor(paramPerso);
				tmp.setAccessible(true);
			} catch (NoSuchMethodException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			//Food food;
			StringBuilder buffer = new StringBuilder();

			for(int i=0; i<FOOD_SOURCE; i++) {
				Point2i p=null;
				if(position.equalsIgnoreCase("random"))
				{
					p = new Point2i(
							rnd.nextInt(env.getWidth()),
							rnd.nextInt(env.getHeight()));
					while (positions.contains(p)) {
						p.set(
								rnd.nextInt(env.getWidth()),
								rnd.nextInt(env.getHeight()));
					}
				}
				else
				{
					String[] tabPosition = position.split(";");
					try
					{
						int x=-1;
						int y=-1;
						//Value inside the env dimension
						if(Integer.valueOf(tabPosition[0])<env.getWidth())
						{
							x=Integer.valueOf(tabPosition[0]);
						}
						if(Integer.valueOf(tabPosition[1])<env.getHeight())
						{
							y=Integer.valueOf(tabPosition[1]);
						}
						if(x!=-1 && y!=-1)
						{
							p = new Point2i(
									Integer.valueOf(tabPosition[0]),
									Integer.valueOf(tabPosition[1]));
							while (positions.contains(p)) {
								p.set(
										rnd.nextInt(env.getWidth()),
										rnd.nextInt(env.getHeight()));
							}
						}
						else
						{
							p = new Point2i(
									rnd.nextInt(env.getWidth()),
									rnd.nextInt(env.getHeight()));
							while (positions.contains(p)) {
								p.set(
										rnd.nextInt(env.getWidth()),
										rnd.nextInt(env.getHeight()));
							}
						}

					}
					catch (NumberFormatException e)
					{
						Log.i("FoodEnvironment", "Position is not a integer, random position");
						p = new Point2i(
								rnd.nextInt(env.getWidth()),
								rnd.nextInt(env.getHeight()));
						while (positions.contains(p)) {
							p.set(
									rnd.nextInt(env.getWidth()),
									rnd.nextInt(env.getHeight()));
						}
					}
				}
				positions.add(p);
				//All good to create
				if(p!=null && tmp!=null)
				{
					Object instance=null;
					try {
						instance = tmp.newInstance(Math.max(10,rnd.nextInt(MAX_FOOD_PER_SOURCE)));
					} catch (InstantiationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					actionApplier.putObject(p.x(), p.y(), (EnvironmentalObject) instance);			
					Class<?> cls2 = cls.getSuperclass();
					Class[] parameterTypes = new Class[0];	
					Method method = null;
					try {
						method = cls2.getDeclaredMethod("getAmount", parameterTypes);
					} catch (NoSuchMethodException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					Object out2 = null;
					try {
						out2 = method.invoke(instance);
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					buffer.setLength(0);
					buffer.append(Locale.getString(MainActivity.class,
							"FOOD_INITIALIZATION PARSER", //$NON-NLS-1$
							Integer.toString(i+1),
							Integer.toString(FOOD_SOURCE),
							Integer.toString(p.x()),
							Integer.toString(p.y()),
							out2));
					System.out.println(buffer.toString());
				}
			}
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			Log.i("FoodEnvironment","Class Not found");
		}

	}

	/** Create a panel which is able to display the world state.
	 * 
	 * @param jaakKernel is the address agent which is managing the Jaak environment.
	 * @param metrics 
	 * @return the created panel.
	 * @throws IllegalStateException if the given jaakKernel does not provide a
	 * {@link GridStateChannel}.
	 */

	public static Panel createPanel(Context context, AgentAddress jaakKernel, int width, int height, int cell_size) {
		GridStateChannel channel = null;
		do
		{
			channel = Kernels.get().getChannelManager().getChannel(jaakKernel, GridStateChannel.class);
		}
		while(channel==null);
		return new Panel(context, channel,width,height,cell_size,ANT_COLONY_COUNT);
	}	



	/**
	 * 
	 * @param colonyId is the identifier of the colony to create.
	 * @param environment is the environment on which the spawner is created.
	 * @param ANT_COLONY_PATROLLER_POPULATION
	 * @param ANT_COLONY_FORAGER_POPULATION
	 * @param type : class name of the colony to create
	 * @param emplacement : position of the colony to create random|x;y
	 * @return an instance of the spawner.
	 */
	public static JaakSpawner createColony(int colonyId, JaakEnvironment environment, int ANT_COLONY_PATROLLER_POPULATION, int ANT_COLONY_FORAGER_POPULATION, String type, String emplacement) {
		ActionApplier actionApplier = environment.getActionApplier();
		Point2i position = new Point2i();
		Random rnd = new Random();
		if(emplacement.equalsIgnoreCase("random"))
		{
			position.set(
					rnd.nextInt(environment.getWidth()),
					rnd.nextInt(environment.getHeight()));
			while (positions.contains(position)) {
				position.set(
						rnd.nextInt(environment.getWidth()),
						rnd.nextInt(environment.getHeight()));
			}
		}
		else
		{
			String[] tabPosition = emplacement.split(";");
			try
			{
				int x=-1;
				int y=-1;
				//Value inside the environment dimension
				if(Integer.valueOf(tabPosition[0])<environment.getWidth())
				{
					x=Integer.valueOf(tabPosition[0]);
				}
				if(Integer.valueOf(tabPosition[1])<environment.getHeight())
				{
					y=Integer.valueOf(tabPosition[1]);
				}
				if(x!=-1 && y!=-1)
				{
					position = new Point2i(
							Integer.valueOf(tabPosition[0]),
							Integer.valueOf(tabPosition[1]));
					while (positions.contains(position)) {
						position.set(
								rnd.nextInt(environment.getWidth()),
								rnd.nextInt(environment.getHeight()));
					}
				}
				else
				{
					position = new Point2i(
							rnd.nextInt(environment.getWidth()),
							rnd.nextInt(environment.getHeight()));
					while (positions.contains(position)) {
						position.set(
								rnd.nextInt(environment.getWidth()),
								rnd.nextInt(environment.getHeight()));
					}
				}

			}
			catch (NumberFormatException e)
			{
				Log.i("CreateColony", "Burrow xml position fail, create random position");
				position = new Point2i(
						rnd.nextInt(environment.getWidth()),
						rnd.nextInt(environment.getHeight()));
				while (positions.contains(position)) {
					position.set(
							rnd.nextInt(environment.getWidth()),
							rnd.nextInt(environment.getHeight()));
				}
			}


		}
		positions.add(position);
		Class<?> cls = null;
		try {
			cls = Class.forName(type);
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Class[] paramPerso = new Class[1];	
		paramPerso[0] = Integer.TYPE;
		Constructor<?> tmp = null;
		try {
			tmp = cls.getDeclaredConstructor(paramPerso);
			tmp.setAccessible(true);
		} catch (NoSuchMethodException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Object instance=null;
		try {
			instance = tmp.newInstance(colonyId);
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		actionApplier.putObject(position.x(), position.y(), (EnvironmentalObject) instance);
		return new AntColonySpawner(ANT_COLONY_PATROLLER_POPULATION, ANT_COLONY_FORAGER_POPULATION, position.x(), position.y());
	}
	
	/**
	 *  Create the ant colonies and the associated spawners.
	 * @param environment is the environment on which the spawner is created.
	 * @param nb_colony : number of the colony to create
	 * @param nb_alreadyCreate : number of colony already create in the env
	 * @param ANT_COLONY_PATROLLER_POPULATION : number of patroller to create in the colony
	 * @param ANT_COLONY_FORAGER_POPULATION : number of forager to create in the colony
	 * @param type : class name of the colony
	 * @param position : position of the colony to create random|x;y
	 */
	public void createColonies(JaakEnvironment environment, int nb_colony,int nb_alreadyCreate, int ANT_COLONY_PATROLLER_POPULATION, int ANT_COLONY_FORAGER_POPULATION, String type, String position) {
		StringBuilder buffer = new StringBuilder();

		for(int i=nb_alreadyCreate; i<nb_alreadyCreate+nb_colony; i++) {
			createdSpawners.add(createColony(i+1, environment, ANT_COLONY_PATROLLER_POPULATION, ANT_COLONY_FORAGER_POPULATION,type,position));
			buffer.setLength(0);
			buffer.append(Locale.getString(MainActivity.class,
					"SPAWNER_INITIALIZATION", //$NON-NLS-1$
					Integer.toString(i+1),
					Integer.toString(createdSpawners.size()),
					Integer.toString(createdSpawners.get(createdSpawners.size()-1).getReferenceSpawningPosition().x()),
					Integer.toString(createdSpawners.get(createdSpawners.size()-1).getReferenceSpawningPosition().y())));
			System.out.println(buffer.toString());
		}
	}
	
	/**
	 * Create the burrow and turtles
	 */
	private void createBurrow()
	{
		for(int i=0;i<listBurrow.size();i++)
		{
			try
			{
				ANT_COLONY_COUNT = ANT_COLONY_COUNT + Integer.valueOf(listBurrow.get(i).getAttribute("quantity"));
			}
			catch (NumberFormatException e)
			{
				ANT_COLONY_COUNT = ANT_COLONY_COUNT + 1;
			}
		}
		int nb_create=0;
		for(int i=0;i<listBurrow.size();i++)
		{
			//Find number of spawns in this burrow
			int numberPatroller = numberPatroller(listBurrow.get(i).getAttribute("id"));
			int numberForager = numberForager(listBurrow.get(i).getAttribute("id"));
			//First burrow create
			if(i!=0)
			{
				nb_create=nb_create+Integer.valueOf(listBurrow.get(i-1).getAttribute("quantity"));
			}
			try
			{
				createColonies(environment, Integer.valueOf(listBurrow.get(i).getAttribute("quantity")), nb_create ,numberPatroller, numberForager,listBurrow.get(i).getAttribute("type"),listBurrow.get(i).getAttribute("position"));
			}
			catch (NumberFormatException e)
			{
				Log.i("Create Burrow", "Quantity isn't integer --> set 1");
				listBurrow.get(i).setAttribute("quantity", "1");
				createColonies(environment, Integer.valueOf(listBurrow.get(i).getAttribute("quantity")), nb_create ,numberPatroller, numberForager,listBurrow.get(i).getAttribute("type"),listBurrow.get(i).getAttribute("position"));
			}

		}
		
		for(int j=0;j<listSpawner.size();j++)
		{
			FindSpawerOuside(listSpawner.get(j));
		}
	}
	
	
	/**
	 *  Recherche des fourmis devant etre creer autre part que dans un burrow
	 * @param element : node information of the spawner
	 */
	private void FindSpawerOuside(Element element)
	{
		String[] profil = xmlLocation(element.getChildNodes(),element.getAttribute("type"),true);
		if(profil!=null)
		{
			if(profil[2].equalsIgnoreCase("oncell"))
			{
				createAnt(this.getJaakEnvironment(), element.getAttribute("type"), profil[0], profil[1],element.getAttribute("quantity"));
			}
		}
	}

}
