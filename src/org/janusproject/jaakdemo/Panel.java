package org.janusproject.jaakdemo;

/* 
 * $Id$
 * 
 * Janus platform is an open-source multiagent platform.
 * More details on <http://www.janus-project.org>
 * Copyright (C) 2010-2012 Janus Core Developers
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

import org.arakhne.afc.math.continous.object2d.Rectangle2f;
import org.arakhne.afc.math.discrete.object2d.Point2i;
import org.arakhne.afc.ui.CenteringTransform;
import org.arakhne.afc.ui.android.zoom.DocumentWrapper;
import org.arakhne.afc.ui.android.zoom.DroidZoomableGraphics2D;
import org.arakhne.afc.ui.android.zoom.ZoomableView;
import org.arakhne.afc.ui.vector.Colors;
import org.arakhne.afc.ui.vector.VectorToolkit;
import org.janusproject.jaak.envinterface.channel.GridStateChannel;
import org.janusproject.jaak.envinterface.channel.GridStateChannelListener;
import org.janusproject.jaak.envinterface.perception.EnvironmentalObject;
import org.janusproject.jaakdemo.environment.Food;
import org.janusproject.jaakdemo.environment.Pheromone;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

/** View of the application.
 * 
 * @author $Author: cdalle$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */

@SuppressLint("ViewConstructor")
public class Panel extends ZoomableView implements GridStateChannelListener {


	private final GridStateChannel channel;
	private int radarLength = 0;
	private org.arakhne.afc.ui.vector.Color[][] grid = null;
	private int[] bases = null;
	private int numberBurrow = 0;
	private int width = 0;
	private int height = 0;
	private int dimX;
	private int dimY;
	private int mapX;
	private int mapY;
	private float focusPreferX = 0;
	private float focusPreferY = 0;	
	private int currentXcenter = 0;
	private int currentYcenter = 0;
	private Canvas canvas = new Canvas();
	public ArrayList<Integer> Couleur = new ArrayList<Integer>();
	public ArrayList<Integer> PosX = new ArrayList<Integer>();
	public ArrayList<Integer> PosY = new ArrayList<Integer>();
	public LinkedList<PointJaak> List = new LinkedList<PointJaak>();
	public boolean Init = false;
	public boolean MiniCarte = true;
	
	/** Color for mobile ants.
	 */
	public static final org.arakhne.afc.ui.vector.Color MOBILE_ANT_COLOR = Colors.GREEN;

	/** Color for mobile ants.
	 */
	public static final org.arakhne.afc.ui.vector.Color IMMOBILE_ANT_COLOR = Colors.YELLOW;

	/** Color for colonies.
	 */
	public static final org.arakhne.afc.ui.vector.Color COLONY_COLOR = Colors.WHITE;

	/** Size of a cell in pixels.
	 */
	public static int CELL_SIZE = 4;

	/**
	 * Class Constructor
	 * @param context of the application
	 * @param channel is the channel from which informations should be retreived.
	 * @param width : width dimension of the device
	 * @param height : height dimension of the device
	 * @param cell_size : representation of one pixel 
	 * @param countBurrow : number of burrow
	 */
	public Panel(Context context, GridStateChannel channel, int width, int height, int cell_size, int countBurrow) {
		// TODO Auto-generated constructor stub
		super(context);
		this.channel = channel;
		this.channel.addGridStateChannelListener(this);
		//device size
		this.dimX = width;
		this.dimY = height;
		//Scale of the mini map of the world
		this.mapX = width*cell_size*20/100;
		this.mapY = height*cell_size*20/100;
		this.CELL_SIZE = cell_size;	
		this.numberBurrow=countBurrow;
	}
	


	/** Replies the color which is corresponding to the given amount
	 * of food.
	 * 
	 * @param amount is the amount of food
	 * @return the color for food
	 */
	public static org.arakhne.afc.ui.vector.Color makeFoodColor(int amount) {
		if (amount<=0) return null;
		int n = 55 + (amount * 200) / XMLParser.MAX_FOOD_PER_SOURCE;
		return VectorToolkit.color(n, 0, 0);
	}

	/** Replies the color which is corresponding to the given amount
	 * of pheromones.
	 * 
	 * @param pheromoneColor is the color of pheromones
	 * @param foodAmount is the amount of food
	 * @return the color for pheromone
	 */
	public static org.arakhne.afc.ui.vector.Color makePheromoneFoodColor(org.arakhne.afc.ui.vector.Color pheromoneColor, int foodAmount) {
		int red = 0;
		if (foodAmount>0) {
			red = 55 + (foodAmount * 200) / XMLParser.MAX_FOOD_PER_SOURCE;
		}
		red += pheromoneColor.getRed();
		if (red<0) red = 0;
		else if (red>255) red = 255;
		
		return VectorToolkit.color(red, pheromoneColor.getGreen(), pheromoneColor.getRed());
	}
	
	/**
	 * Start Jaak
	 */
	@Override
	public synchronized void jaakStart() {
		//world size
		this.width = this.channel.getGridWidth();
		this.height = this.channel.getGridHeight();
		this.grid = new org.arakhne.afc.ui.vector.Color[this.width][this.height];
	}
	
	/**
	 * Evolution of the Jaak environment
	 */
	@Override
	public synchronized void gridStateChanged() {
		// If initialisation of grid isn't done 
		if(this.width==0)
		{
			jaakStart();
		}
	
		Iterable<EnvironmentalObject> iterable;
		Iterator<EnvironmentalObject> iterator;
		EnvironmentalObject obj;
		Food food;
		org.arakhne.afc.ui.vector.Color c;
		float speed;
		List = new LinkedList<PointJaak>();
		for(int x=0; x<this.width; ++x) {
			for(int y=0; y<this.height; ++y) {
				speed = this.channel.getSpeed(x,y);
				if (!Float.isNaN(speed)) {
					if (speed>0f) {
						c = MOBILE_ANT_COLOR;
					}
					else {
						c = IMMOBILE_ANT_COLOR;
					}
				}
				else {
					iterable = this.channel.getEnvironmentalObjects(x, y);
					c =null;
					if (iterable!=null) {
						iterator = iterable.iterator();
						int nbElts = 0;
						int pc;
						food = null;
						int cr = 0;
						int cg = 0;
						int cb = 0;
						while (iterator.hasNext()) {
							obj = iterator.next();
							if (obj instanceof Pheromone) {
								pc = -((Pheromone)obj).getColor();
								cr = Math.max(pc, cr);
								cg = Math.max(pc, cg);
								cb = Math.max(pc, cb);
								++nbElts;
							}
							else if (obj instanceof Food && food==null) {
								food = (Food)obj;
							}
						}
						
						if (nbElts>0) {
							c = VectorToolkit.color(cr, cg, cb);
						}
						if (c!=null && food!=null) {
							c = makePheromoneFoodColor(c, food.intValue());
						}
						else if (food!=null) {
							c = makeFoodColor(food.intValue());
						}
					}
				}
				if (c!=null) 
				{
					List.add(new PointJaak(x,y,c));
				}
				
				if (c==Colors.BLACK) 
				{
					//Background of the application
					this.grid[x][y] = Colors.BLACK;
					
				}	
				this.grid[x][y] = c;
			}
		}
		
		if (this.bases==null) {
			Point2i[] ptIterator = this.channel.getSpawningPositions();
			// Get only burrow
			this.bases = new int[numberBurrow*2];
			for(int i=0,j=0; i<this.bases.length; i+=2,j++) {
				this.bases[i] = ptIterator[j].x();
				this.bases[i+1] = ptIterator[j].y();
			}
		}
	}
	
	/**
	 * End of the Jaak simulation 
	 */
	@Override
	public synchronized void jaakEnd() {
		this.channel.removeGridStateChannelListener(this);
		this.bases = null;
		this.grid = null;
		this.width = this.height = 0;
	}
	
	/**
	 * Representation of one pixel in the simulation 
	 * @param x coordinate of the pixel
	 * @return coordinate of the pixel in the screen
	 */
	private static int simu2screen_x(int x) {
		return x * CELL_SIZE + 5;
	}
	/**
	 * Representation of one pixel in the simulation 
	 * @param y coordinate of the pixel
	 * @return coordinate of the pixel in the screen
	 */
	private static int simu2screen_y(int y) {
		return y * CELL_SIZE + 5;
	}
	/**
	 * Representation of one pixel in the screen 
	 * @param x coordinate of the pixel
	 * @return coordinate of the pixel in the simulation
	 */
	private static int screen2simu_x(int x) {
		return ((x - 5) / CELL_SIZE); 
	}
	/**
	 * Representation of one pixel in the screen 
	 * @param x coordinate of the pixel
	 * @return coordinate of the pixel in the simulation
	 */
	private static int screen2simu_y(int y) {
		return ((y - 5) / CELL_SIZE); 
	}
		
	/**
	 * return position of the base to be center of the screen
	 * @param numero of the base burrow
	 * @return position of this burrow
	 */
	public Point2i getPositionBase(int numero)
	{
		if(this.bases.length!=0)
		{
			int x = this.bases[(numero-1)*2];
			int y = this.bases[((numero-1)*2)+1];
			Point2i P = new Point2i(simu2screen_x(x), simu2screen_y(y)); 
			return P;
		}
		else
		{
			return new Point2i(0,0);
		}
	}
	
	/**
	 * Localize the position of the base in the minimap
	 * @param Position of the burrow
	 * @param width_height 1 if position is the x coordinate / 0 for the y coordinate
	 * @return x or y position in the minimap
	 */
	private int smallMapPosition(float Position, int width_height)
	{
		int out = 0;
		//width_height --> 1 if it's for a x position 0 for y position
		switch(width_height)
		{
			case 1: out = (int) (Position*this.mapX/this.width);
				break;
			case 0: out = (int) (Position*this.mapY/this.height);
				break;
		}
		return out;
		
	}
	
	/**
	 * localize the position of the point view on the minimap
	 * @param Position in the device
	 * @param width_height 1 if position is the x coordinate / 0 for the y coordinate
	 * @return x or y position in the minimap
	 */
	private int smallMapDevicePosition(float Position, int width_height)
	{
		//Position is a float between -inf and inf because the view isn't defined
		if(Position<0)
		{
			return 0;
		}
		else
		{
			int out = 0;
			//width_height --> 1 if it's for a x position 0 for y position
			switch(width_height)
			{
				case 1:	out = (int) (Position*this.mapX/this.width);
					if(out>this.mapX)
					{
						out=this.mapX;
					}
					break;
				case 0:	out = (int) (Position*this.mapY/this.height);
					if(out>this.mapY)
					{
						out=this.mapY;
					}
					break;
			}
			return out;
		}
	}
	

	@Override
	protected DocumentWrapper createDocumentWrapper() {
		// TODO Auto-generated method stub
		return null;
	}


	//set the preferred focus for the resetview
	public void setPreferredFocus(float X, float Y)
	{
		focusPreferX = X;
		focusPreferY = Y;
	}
	

	@Override
	protected float getPreferredFocusX() {
		// TODO Auto-generated method stub
		return focusPreferX;
	}



	@Override
	protected float getPreferredFocusY() {
		// TODO Auto-generated method stub
		return focusPreferY;
	}
	protected Canvas getCanvas()
	{
		return canvas;
	}

	/**
	 * Function to draw information
	 */
	@Override
	protected synchronized void onDrawView(Canvas canvas, float scaleFactor,
			CenteringTransform centeringTransform) {
		
		DroidZoomableGraphics2D g = new DroidZoomableGraphics2D(
				canvas,
				Colors.BLACK,
				Colors.WHITE,
				scaleFactor,
				centeringTransform,
				Colors.BLACK,
				false, //isAntiAlias,
				getScalingSensitivity(),
				getFocusX(),
				getFocusY(),
				getMinScalingFactor(),
				getMaxScalingFactor());

		
		this.setBackgroundColor(Color.BLACK);
		if(!Init && this.bases!=null)
		{
			Init = true;
			//center on the world
			setFocusPoint(simu2screen_x(this.width/2), simu2screen_y(this.height/2));
		}
		g.setOutlineDrawn(false);
	
		//Draw pixel
		if(!List.isEmpty())
		{
			Iterator<PointJaak> ite = List.iterator();
			while(ite.hasNext())
			{
				PointJaak iterator = ite.next();
				g.setFillColor((iterator.getColor()));
				g.draw(new Rectangle2f(simu2screen_x(iterator.getX()),simu2screen_y(iterator.getY()),CELL_SIZE,CELL_SIZE));
			}
		}
		
		//Draw burrow
		if (this.bases!=null) {
			g.setInteriorPainted(true);
			g.setOutlineDrawn(true);
			g.setFillColor(Colors.WHITE);
			int x, y;
			int s = this.radarLength*2 + 1;
			for(int i=0; i<this.bases.length-1; i+=2) {
				
				x = this.bases[i];
				y = this.bases[i+1];
				g.draw(new Rectangle2f(simu2screen_x(x), simu2screen_y(y), CELL_SIZE, CELL_SIZE));
				if (this.radarLength>0) {
					g.setInteriorPainted(false);
					g.setOutlineDrawn(true);
					g.setFillColor(Colors.WHITE);
				}
			}
			this.radarLength = (this.radarLength + 1) % 15;
		}
		
		
		if(MiniCarte)
		{
			//Draw map
			drawMiniMap(canvas);
		}

    	invalidate();
		
	}
	
	/**
	 * Fucntion to draw mimimap
	 * @param c canvas of the application
	 */
	protected void drawMiniMap(Canvas c)
	{
		Paint paint = new Paint();
		paint.setColor(Color.WHITE);
		paint.setStyle(Paint.Style.STROKE);
		//Limit of the mini map
		c.drawRect(new RectF(1, 1, this.mapX, this.mapY), paint);
		//background of the mini map
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(Color.BLACK);
		c.drawRect(new RectF(2, 2, this.mapX-2, this.mapY-2), paint);
		//Draw burrow on mini map
		paint.setColor(Color.WHITE);
		if (this.bases!=null) {
			for(int i=0; i<this.bases.length-1; i+=2) {
				c.drawCircle(smallMapPosition(this.bases[i],1), smallMapPosition(this.bases[i+1],0), 10, paint);
			}
		}
		//Draw visualization point of the middle point view on the mini map
		if(this.width!=0)
		{
			paint.setColor(Color.RED);
			c.drawCircle(smallMapDevicePosition(screen2simu_x((int)getFocusX()), 1), smallMapDevicePosition(screen2simu_y((int)getFocusY()), 0), 10, paint);
		
		}
	}
	
}


