package org.janusproject.jaakdemo;

/* 
 * $Id$
 * 
 * Janus platform is an open-source multiagent platform.
 * More details on <http://www.janus-project.org>
 * Copyright (C) 2010-2012 Janus Core Developers
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


import org.arakhne.afc.ui.vector.Color;

/** PointJaak. Position of the pixel to color 
 * 
 * @author $Author: cdalle$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */

/**Pixel to be draw
 * Burrow/Pheromone/Food/Ant
 */
public class PointJaak {
	private int X;
	private int Y;
	private Color color;
	/**
	 * Class constructor
	 * @param x-position in the environment
	 * @param y-position in teh environment
	 * @param c color of the pixel
	 */
	public PointJaak(int x, int y, Color c)
	{
		X=x;
		Y=y;
		color=c;
	}
	public int getX() {
		return X;
	}
	public void setX(int x) {
		X = x;
	}
	public int getY() {
		return Y;
	}
	public void setY(int y) {
		Y = y;
	}
	public Color getColor() {
		return color;
	}
	public void setColor(Color color) {
		this.color = color;
	}

	
}
