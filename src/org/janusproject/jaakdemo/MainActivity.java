package org.janusproject.jaakdemo;

/* 
 * $Id$
 * 
 * Janus platform is an open-source multiagent platform.
 * More details on <http://www.janus-project.org>
 * Copyright (C) 2010-2012 Janus Core Developers
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.ParserConfigurationException;

import org.arakhne.afc.math.discrete.object2d.Point2i;
import org.janusproject.jaak.kernel.JaakKernel;
import org.janusproject.jaak.kernel.JaakKernelController;
import org.janusproject.kernel.agent.Kernels;
import org.xml.sax.SAXException;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/** Activity of the project
 * 
 * @author $Author: cdalle$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */

@SuppressLint("NewApi")
public class MainActivity extends Activity{

	DisplayMetrics metrics = new DisplayMetrics();
	
	//Test if the simulation is launch
	private boolean Simulaunch = false;
	//Test if the simulation is running
	private boolean SimuRun = true;
	//Device parameters (width/height)
	Point size;
	//Main class
	private Panel mPanel = null;
	//Class to parse XML File and stock the information
	private XMLParser mParser = null;
	//Default name XML File to read
	private String XMLname = "JaakDemo";
	//Test if a XML File is already load
	private boolean loadComplete = false;
	//Current XML File to load
	TextView Textview =null;
	//Control if menu button is present on the device
	private boolean menuButton = true;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//Information about device used
		setContentView(R.layout.accueil);
		Textview = (TextView) findViewById(R.id.txtXml);
		Textview.setText("XML File : "+ XMLname);
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		Display display = getWindowManager().getDefaultDisplay();
		size = new Point();
		display.getSize(size);
		if(Build.VERSION.SDK_INT <= 10 || (Build.VERSION.SDK_INT >= 14 && ViewConfiguration.get(this).hasPermanentMenuKey()))
		{
			 // menu key is present
			 menuButton =true;
		}
		else
		{
			//No menu key
			menuButton =false;
		}


		
	}
	
	/**
	 * Function called when button was clicked in layout.accueil to load XML File 
	 * @param view is the current view when button is clicked
	 */
	public void loading(View view) {
		//Create dialog box to get the name of XML File 
		AlertDialog.Builder builderSingle = new AlertDialog.Builder(MainActivity.this);
        builderSingle.setIcon(R.drawable.ic_launcher);
        builderSingle.setTitle("XML � charger ?");
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MainActivity.this,android.R.layout.select_dialog_singlechoice);
       	arrayAdapter.add("JaakDemo");
	    arrayAdapter.add("JaakDemoV2");
	    arrayAdapter.add("JaakDemoV3");
        builderSingle.setNegativeButton("cancel",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builderSingle.setAdapter(arrayAdapter,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    	//Name of the XML to take
                    	mParser.ANT_COLONY_COUNT=0;
                        XMLname = arrayAdapter.getItem(which);
                        stringXMLName(XMLname);
                        Textview.setText("XML File : "+ XMLname);
                    }
                });
        builderSingle.show();
		
	}
	
	/**
	 * Parse XML File with the input name
	 * @param name of the xml file to load
	 */
	public void stringXMLName(String name) {
		//Class to parse XML File given by user
		mParser = new XMLParser(getApplicationContext()); 
		//Read information of the simulation on the XML file
		InputStream in_s = null;
		try {
			in_s = getApplicationContext().getAssets().open(name+".xml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Read XML File
		try {
			mParser.readXML(in_s,size);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		loadComplete = true;
	}

	/**
	 * Generate menu information given by xml file in res menu
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		
		if(menuButton)
		{
			getMenuInflater().inflate(R.menu.main, menu);
		}
		else
		{
			getMenuInflater().inflate(R.menu.menufixed, menu);
		}
		return true;
	}
	
	/**
	 * Function called when menu button is clicked
	 * @param item is the current button menu clicked
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		
        switch (item.getItemId()) {
           case R.id.start:
        	   if(!Simulaunch)
       		   {
	        	   setContentView(R.layout.activity_main);
	        	   //Default configuration to run
	    		   if(!loadComplete)
	    		   {
	    			   stringXMLName(XMLname);
	    		   }
	        	   JaakKernelController controller = JaakKernel.initializeKernel(mParser.getJaakEnvironment(),mParser.getJaakSpawners());
	    		   mPanel = XMLParser.createPanel(this, 
	    				   controller.getKernelAddress(),size.x/mParser.CELL_SIZE,size.y/mParser.CELL_SIZE,mParser.CELL_SIZE);
	    		   setContentView(mPanel);
	    		   Simulaunch = true;
	    		   item.setTitle(R.string.Stop);
       		   }
        	   else
        	   {
        		   //TODO: Must be done with JaakKernelController
            	   Kernels.killAll();
            	   Simulaunch = false;
            	   setContentView(R.layout.accueil);
            	   item.setTitle(R.string.Start);
            	   Textview.setText("XML File : "+ XMLname);
            	   loadComplete=false;
        	   }
        	   return true;
           case R.id.pause:
        	   //TODO: Must be done with JaakKernelController
        	   if(Simulaunch)
        	   {
	        	   if(SimuRun)
	        	   {
	        		   Kernels.get().pause();
	        		   SimuRun = false;
	        		   item.setTitle(R.string.Resume);
	           	   }
	        	   else
	        	   {
	        		   Kernels.get().resume();
	        		   SimuRun = true;
	        		   item.setTitle(R.string.Pause);
	          	   }
        	   }
           	   return true;
           case R.id.recherche:
        	   //Look for the burrow that user select
        	   if(Simulaunch)
        	   {    	    		   
        		   AlertDialog.Builder builderSingle = new AlertDialog.Builder(MainActivity.this);
                   builderSingle.setIcon(R.drawable.ic_launcher);
                   builderSingle.setTitle("Burrow number ?");
                   final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MainActivity.this,android.R.layout.select_dialog_singlechoice);
                   for(int i = 0 ; i <mParser.ANT_COLONY_COUNT;i++)
                   {
                	   arrayAdapter.add(String.valueOf(i+1));
                   }
                   
                   builderSingle.setNegativeButton("cancel",
                           new DialogInterface.OnClickListener() {

                               @Override
                               public void onClick(DialogInterface dialog, int which) {
                                   dialog.dismiss();
                               }
                           });

                   builderSingle.setAdapter(arrayAdapter,
                           new DialogInterface.OnClickListener() {

                               @Override
                               public void onClick(DialogInterface dialog, int which) {
                                   String strName = arrayAdapter.getItem(which);
                                   int i = Integer.parseInt(strName);
        		        	       if(i<=mParser.ANT_COLONY_COUNT && i>0)
        		        	       {
        		        	           Point2i tmp = mPanel.getPositionBase(i);
        		        	           mPanel.setPreferredFocus(tmp.getX(), tmp.getY());
        			            	   mPanel.setFocusPoint(tmp.getX(),tmp.getY());
        			            	   mPanel.resetView();
        		        	       }
                               }
                           });
                   builderSingle.show();
        	   }
              return true;
          case R.id.carte:
        	   //Swap Minimap visibility 
        	  if(Simulaunch)
       	   	  { 
	        	   if(mPanel.MiniCarte)
	               {
	            	   mPanel.MiniCarte = false;
	               }
	               else
	               {
	            	   mPanel.MiniCarte = true;
	               }
       	   	  }
               return true;
          case R.id.quitter:
              finish();
              return true;
        }
        return false;
    }
	
	protected void onDestroy(){
		super.onDestroy();
		//TODO: Must be done with JaakKernelController
		Kernels.killAll();
	}
		

}
